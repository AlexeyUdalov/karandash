# Gulp-pug-sass-boilerplate

#### Установка
Для работы со сборкой проtкта необходим **[node.js](https://nodejs.org/en/)**

```sh
$ npm i
```

Для просмотра проекта:
```sh
$ gulp
```

Билд:
```sh
$ gulp build
```

Исходники проекта содержаться в папке **/dist**