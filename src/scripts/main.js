const APP = {
	// сет брейкпоинтов для js
	// должны совпадать с теми что в body:after
	mediaBreakpoint: {
		sm: 576,
		md: 768,
		lg: 992,
		xl: 1200
	},
	cache: {},
	initBefore: function() {
		APP.polyfills()
		APP.svgIcons()
		document.documentElement.className =
			document.documentElement.className.replace("no-js", "js");
	},

	init: function() {
		APP.detectIE();
		APP.lazyload();

		const myMask = new APP.Mask(".js-tel")
		myMask.init()

		APP.buttons();
		APP.closeOnFocusLost();
		APP.initAlbumSlider();
		APP.inintVideoSlider();
		APP.initVideoPopup();
		APP.scrollWindow();
		APP.inintPlayer();
	},

	initOnLoad: function() {
		APP.truncateText( document.querySelectorAll('.js-dot') );
	},

	buttons: function() {
		let showTextButton = document.querySelector('.js-show-text-btn');
		if (showTextButton) {
			showTextButton.addEventListener('click', function (e) {
				e.preventDefault();
				let text = $('.js-text');
				text.slideToggle();
				$(e.currentTarget).toggleClass('active');
			});
		}
		Array.prototype.forEach.call(document.querySelectorAll('.js-show-palyer'), function (button) {
			button.addEventListener('click', function (e) {
				e.preventDefault();
				let id ='#' + $(e.currentTarget).data('id'),
					players = $('.player');

				if (players.filter('.active').length) {
					players.removeClass('active').slideUp(500);
					setTimeout(function () { players.filter(id).addClass('active').slideDown(500); }, 600);
				} else {
					players.filter(id).addClass('active').slideDown(500)
				}

				players.filter(id).find('.js-play-song').first().click();

			});
		});
	},

	closeOnFocusLost: function() {
		document.addEventListener('click', function(e) {
			const trg = e.target;
			if (!trg.closest(".header")) {
				document.body.classList.remove('nav-showed');
			}
		});
	},

	Mask: function(selector, regExpString = "+7 (___) ___-__-__") {
		this.elements = document.querySelectorAll(selector)
		this.init = function() {
			listeners(this.elements)
		}

		const listeners = (selector) => {
			for (let i = 0; i < selector.length; i++) {
				const input = selector[i];
				input.addEventListener("input", mask, false);
				input.addEventListener("focus", mask, false);
				input.addEventListener("blur", mask, false);
			}
		}

		const setCursorPosition = function(pos, elem) {
			elem.focus();
			if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
			else if (elem.createTextRange) {
				const range = elem.createTextRange();
				range.collapse(true);
				range.moveEnd("character", pos);
				range.moveStart("character", pos);
				range.select()
			}
		}


		const mask = function(event) {
			var matrix = regExpString,
				i = 0,
				def = matrix.replace(/\D/g, ""),
				val = this.value.replace(/\D/g, "");
			if (def.length >= val.length) val = def;
			this.value = matrix.replace(/./g, function(a) {
				return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
			});
			if (event.type == "blur") {
				if (this.value.length == 2) this.value = ""
			} else {
				setCursorPosition(this.value.length, this)
			}
		}

	},

	truncateText: function(selector) {
		const cutText = () => {
			for (let i = 0; i < selector.length; i++) {
				const text = selector[i]
				const elemMaxHeight = parseInt(getComputedStyle(text).maxHeight, 10)
				const elemHeight = text.offsetHeight
				const maxHeight = elemMaxHeight ? elemMaxHeight : elemHeight
				shave(text, maxHeight);
			}
		}

		APP.cache.cutTextListener = APP.throttle(cutText, 100)

		cutText();

		window.addEventListener('resize', APP.cache.cutTextListener);
	},

	lazyload: function() {
		if (typeof APP.myLazyLoad == 'undefined') {
			_regularInit()
		} else {
			_update()
		}

		function _update() {
			APP.myLazyLoad.update();
			APP.objectFitFallback(document.querySelectorAll('[data-object-fit]'));
		}

		function _regularInit() {
			APP.myLazyLoad = new LazyLoad({
				elements_selector: ".lazyload",
				callback_error: function(el) {
					el.parentElement.classList.add('lazyload-error')
				}
			});
			APP.objectFitFallback(document.querySelectorAll('[data-object-fit]'));
		}
	},

	objectFitFallback: function(selector) {
		// if (true) {
		if ('objectFit' in document.documentElement.style === false) {
			for (let i = 0; i < selector.length; i++) {
				const that = selector[i]
				const imgUrl = that.getAttribute('src') ? that.getAttribute('src') : that.getAttribute('data-src');
				const dataFit = that.getAttribute('data-object-fit')
				let fitStyle
				if (dataFit === 'cover') {
					fitStyle = 'cover'
				} else {
					fitStyle = 'contain'
				}
				const parent = that.parentElement
				if (imgUrl) {
					parent.style.backgroundImage = 'url(' + imgUrl + ')'
					parent.classList.add('fit-img')
					parent.classList.add('fit-img--'+fitStyle)
				}
			}
		}
	},

	svgIcons: function () {
		const container = document.querySelector('[data-svg-path]')
		const path = container.getAttribute('data-svg-path')
		const xhr = new XMLHttpRequest()
		xhr.onload = function() {
			container.innerHTML = this.responseText
		}
		xhr.open('get', path, true)
		xhr.send()
	},

	polyfills: function () {
		/**
		 * polyfill for elem.closest
		 */
		(function(ELEMENT) {
			ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
			ELEMENT.closest = ELEMENT.closest || function closest(selector) {
				if (!this) return null;
				if (this.matches(selector)) return this;
				if (!this.parentElement) {return null}
					else return this.parentElement.closest(selector)
				};
		}(Element.prototype));

		/**
		 * polyfill for elem.hasClass
		 */
		Element.prototype.hasClass = function(className) {
			return this.className && new RegExp("(^|\\s)" + className + "(\\s|$)").test(this.className);
		};
	},

	detectIE: function() {
		/**
		 * detect IE
		 * returns version of IE or false, if browser is not Internet Explorer
		 */

		 (function detectIE() {
		 	var ua = window.navigator.userAgent;

		 	var msie = ua.indexOf('MSIE ');
		 	if (msie > 0) {
				// IE 10 or older => return version number
				var ieV = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
				document.querySelector('body').className += ' IE';
			}

			var trident = ua.indexOf('Trident/');
			if (trident > 0) {
				// IE 11 => return version number
				var rv = ua.indexOf('rv:');
				var ieV = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
				document.querySelector('body').className += ' IE';
			}

			var edge = ua.indexOf('Edge/');
			if (edge > 0) {
				// IE 12 (aka Edge) => return version number
				var ieV = parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
				document.querySelector('body').className += ' IE';
			}

			// other browser
			return false;
		})();
	},

	throttle: function (callback, limit) {
		var wait = false;
		return function() {
			if (!wait) {
				callback.call();
				wait = true;
				setTimeout(function() {
					wait = false;
				}, limit);
			}
		};
	},

	getScreenSize: function() {
		let screenSize =
			window
			.getComputedStyle(document.querySelector('body'), ':after')
			.getPropertyValue('content');
		screenSize = parseInt(screenSize.match(/\d+/));
		return screenSize;
	},

	initAlbumSlider: function() {
		let slider = document.querySelector('.js-album-slider');
		if (slider) {
			new Swiper (slider, {
				initialSlide: 2,
				slidesPerView: 5,
				spped: 1200,
				setWrapperSize: true,
				centeredSlides: true,
				touchEventsTarget: 'wrapper',
				navigation: {
					nextEl: '.js-album-next',
					prevEl: '.js-album-prev'
				},
				breakpoints: {
					767: {
						slidesPerView: 1
					},
					1199: {
						slidesPerView: 2
					}
				}
			});
		}
	},

	inintVideoSlider: function() {
		let slider = document.querySelector('.js-video-slider');
		if (slider) {
			new Swiper (slider, {
				initialSlide: 1,
				slidesPerView: 1,
				spaceBetween: 16,
				centeredSlides: true,
				touchEventsTarget: 'wrapper',
				navigation: {
					nextEl: '.js-video-next',
					prevEl: '.js-video-prev'
				},
			});
		}
	},

	initVideoPopup: function() {
		const closeSpeed = 200;
		$(document).on('click', '.js-youtube', function (e) {
			const $this = $(this)
			e.preventDefault();
			$.magnificPopup.open({
				items: {
					src: $this.attr('href')
				},
				disableOn: 700,
				type: 'iframe',
				mainClass: 'mfp-fade mfp-bg-dark',
				removalDelay: closeSpeed,

				fixedContentPos: true,
				fixedBgPos: true,
			});
		});
	},

	scrollWindow: function() {
		let links = document.querySelectorAll('.js-anchor'),
			speed = 0.3;
		if (links) {
			Array.prototype.forEach.call(links, function (link) {
				link.addEventListener('click', function (e) {
					e.preventDefault();
					let windowScroll = window.pageYOffset,
						hash = this.href.replace(/[^#]*(.*)/, '$1'),
						blockTop = document.querySelector(hash).getBoundingClientRect().top,
						start = null;

					requestAnimationFrame(step);

					function step(time) {
						if (start === null) start = time;
						let progress = time - start,
							r = (blockTop < 0 ? Math.max(windowScroll - progress / speed, windowScroll + blockTop) : Math.min(windowScroll + progress / speed, windowScroll + blockTop));
						window.scrollTo(0, r);
						if (r != windowScroll + blockTop) {
							requestAnimationFrame(step);
						} else {
							location.hash = hash;
						}
					}
				});
			});
		}
	},
	playersData: {},
	inintPlayer: function() {

		$(function () {
			// Setup the player to autoplay the next track
			var a = audiojs.createAll({
				trackEnded: function () {
					let list = $(audio[APP.playersData.index].wrapper).next('ol'),
						button = list.find('li.playing').find('.js-play-song'),
						next = list.find('li.playing').next();
					if (!next.length) next = list.find('li:first');
					next.addClass('playing').siblings().removeClass('playing');
					button.removeClass('playing');
					next.find('.js-play-song').addClass('playing');
					audio[APP.playersData.index].load($('a', next).attr('data-src'));
					audio[APP.playersData.index].play();
				}
			});

			// Load in the first track
			var audio = a;
			APP.playersData.palyers = audio;
			$.each(audio, function (index, el) {
				let first = $(el.wrapper).next('ol').find('li:first').find('a[data-src]').attr('data-src');
				el.load(first);
			});

			// Load in a track on click
			$('ol li .js-play-song').click(function (e) {
				e.preventDefault();
				let button = $(this),
					index = button.closest('ol').index('ol'),
					li = button.parent();

				if (!button.hasClass('playing') && !button.hasClass('pause')) {
					if (APP.activePlayer.hasOwnProperty('index') && APP.activePlayer.index !== index) {
						let audioActive = audio[APP.activePlayer.index],
							buttonActive = $(audioActive.wrapper).next('ol').find('.playing').find('.js-play-song');
						if (!buttonActive.hasClass('pause')) {
							audioActive.playPause();
						}
						$(audioActive.wrapper).next('ol').find('.playing').removeClass('playing');
						$(audioActive.wrapper).next('ol').find('.pause').removeClass('pause');
					}
					APP.activePlayer.index = index;
					$(audio[index].wrapper).next('ol').find('.playing').removeClass('playing');
					$(audio[index].wrapper).next('ol').find('.pause').removeClass('pause');
					li.addClass('playing');
					audio[index].load(li.find('a[data-src]').attr('data-src'));
					audio[index].play();

					button.addClass('playing');
				} else if (button.hasClass('playing')) {
					button.removeClass('playing').addClass('pause');
					audio[index].playPause();
				} else if (button.hasClass('pause')) {
					button.removeClass('pause').addClass('playing');
					audio[index].play();
				}
			});
		});
	}
};

APP.initBefore();

document.addEventListener('DOMContentLoaded', function() {
	APP.init();
});

window.onload = function() {
	APP.initOnLoad();
};
